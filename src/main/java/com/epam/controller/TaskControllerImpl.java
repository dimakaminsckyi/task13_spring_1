package com.epam.controller;

import com.epam.config.BeanConfigOne;
import com.epam.model.bean.BeanA;
import com.epam.model.bean.BeanF;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class TaskControllerImpl implements TaskController {

    private static Logger log = LogManager.getLogger(TaskControllerImpl.class);

    @Override
    public void runContext() {
        ConfigurableApplicationContext context = new AnnotationConfigApplicationContext(BeanConfigOne.class);
        BeanA beanBC = context.getBean("getBeanBC", BeanA.class);
        BeanA beanDC = context.getBean("getBeanBD", BeanA.class);
        BeanA beanCD = context.getBean("getBeanCD", BeanA.class);
        BeanF beanF = context.getBean(BeanF.class);
        log.info(beanBC.getName() + " " + beanBC.getValue());
        log.info(beanDC.getName() + "  " + beanDC.getValue());
        log.info(beanCD.getName() + " " + beanCD.getValue());
        context.close();
    }
}
