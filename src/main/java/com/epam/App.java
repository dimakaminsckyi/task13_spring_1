package com.epam;

import com.epam.config.BeanConfigOne;
import com.epam.model.bean.BeanA;
import com.epam.model.bean.BeanF;
import com.epam.view.MainView;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {

    public static void main(String[] args) {
        new MainView().runMenu();
    }
}
