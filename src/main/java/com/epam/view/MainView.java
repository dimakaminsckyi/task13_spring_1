package com.epam.view;

import com.epam.controller.TaskController;
import com.epam.controller.TaskControllerImpl;
import com.epam.view.util.PropertiesRead;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Scanner;

public class MainView {

    private static Logger log = LogManager.getLogger(MainView.class);
    private PropertiesRead read = new PropertiesRead();
    private TaskController controller = new TaskControllerImpl();
    private Map<String, String> menu;
    private Map<String, Runnable> methodRun;

    public MainView() {
        initMenu();
        methodRun = new LinkedHashMap<>();
        methodRun.put("run", () -> controller.runContext());
        methodRun.put("exit", () -> log.info("Exit"));
    }

    public void runMenu() {
        Scanner scanner = new Scanner(System.in);
        StringBuilder flagExit = new StringBuilder("");
        do {
            menu.values().forEach(log::info);
            try {
                flagExit.setLength(0);
                flagExit.append(scanner.nextLine());
                methodRun.get(flagExit.toString()).run();
            } catch (NullPointerException e) {
                log.error("Input correct option");
            }
        } while (!flagExit.toString().equals("exit"));
    }

    private void initMenu() {
        menu = new LinkedHashMap<>();
        menu.put("run", read.getValue("run"));
        menu.put("exit", "exit - Exit");
    }
}


