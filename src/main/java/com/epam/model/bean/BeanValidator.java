package com.epam.model.bean;

public interface BeanValidator {
    void validate();
}
