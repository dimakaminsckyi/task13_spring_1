package com.epam.model.bean;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.lang.NonNull;

public class BeanD implements BeanValidator {

    private static Logger log = LogManager.getLogger(BeanD.class);
    @NonNull
    @Value("${beanD.name}")
    private String name;
    @Value("${beanD.value}")
    private int value;

    public BeanD() {
    }

    @Override
    public void validate() {
        if (getValue() >= 0) {
            log.info("Validate successful : D");
        } else {
            log.error("Validate error");
        }
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public void destroy() {
        log.info("Destroy D");
    }

    public void init() {
        log.info("init D");
    }

    @Override
    public String toString() {
        return "BeanD{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
