package com.epam.model.bean;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.lang.NonNull;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class BeanE implements BeanValidator {

    private static Logger log = LogManager.getLogger(BeanE.class);
    @NonNull
    private String name;
    private int value;

    public BeanE() {
    }

    public BeanE(BeanA beanBC) {
        this.name = beanBC.getName();
        this.value = beanBC.getValue();
    }

    @Override
    public void validate() {
        if (getValue() >= 0) {
            log.info("Validate successful : A");
        } else {
            log.error("Validate error");
        }
    }

    @PostConstruct
    public void postConstruct() {
        log.info("Post Construct : E");
    }

    @PreDestroy
    public void preDestroy() {
        log.info("Pre Destroy : E");
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "BeanE{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
