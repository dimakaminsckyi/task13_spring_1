package com.epam.model.bean;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.lang.NonNull;

public class BeanA implements BeanValidator, InitializingBean, DisposableBean {

    private static Logger log = LogManager.getLogger(BeanA.class);
    @NonNull
    private String name;
    private int value;

    public BeanA() {
    }

    public BeanA(BeanB beanB, BeanC beanC) {
        this.name = beanB.getName();
        this.value = beanC.getValue();
    }

    public BeanA(BeanB beanB, BeanD beanD) {
        this.name = beanB.getName();
        this.value = beanD.getValue();
    }

    public BeanA(BeanC beanC, BeanD beanD) {
        this.name = beanC.getName();
        this.value = beanD.getValue();
    }

    @Override
    public void validate() {
        if (getValue() >= 0) {
            log.info("Validate successful : A");
        } else {
            log.error("Validate error");
        }
    }

    @Override
    public void destroy() {
        log.info("DisposableBean :  Bean A");
    }

    @Override
    public void afterPropertiesSet() {
        log.info("After properties Set : Bean A");
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
