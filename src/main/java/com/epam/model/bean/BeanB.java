package com.epam.model.bean;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.lang.NonNull;

public class BeanB implements BeanValidator {

    private static Logger log = LogManager.getLogger(BeanB.class);
    @NonNull
    @Value("${beanB.name}")
    private String name;
    @Value("${beanB.value}")
    private int value;

    public BeanB() {
    }

    @Override
    public void validate() {
        if (getValue() >= 0) {
            log.info("Validate successful : B");
        } else {
            log.error("Validate error");
        }
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public void destroy() {
        log.info("Destroy B");
    }

    public void init() {
        log.info("init B");
    }

    public void changeInitMethod() {
        log.info("Text From changed init method" + getClass().getSimpleName());
    }

    @Override
    public String toString() {
        return "BeanB{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
