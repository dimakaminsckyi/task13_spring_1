package com.epam.model.bean;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.lang.NonNull;

public class BeanC implements BeanValidator {

    private static Logger log = LogManager.getLogger(BeanC.class);
    @NonNull
    @Value("${beanC.name}")
    private String name;
    @Value("${beanC.value}")
    private int value;

    public BeanC() {
    }

    @Override
    public void validate() {
        if (getValue() >= 0) {
            log.info("Validate successful : C");
        } else {
            log.error("Validate error");
        }
    }

    public void destroy() {
        log.info("Destroy C");
    }

    public void init() {
        log.info("init C");
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "BeanC{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
