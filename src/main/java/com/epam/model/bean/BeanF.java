package com.epam.model.bean;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.lang.NonNull;

public class BeanF implements BeanValidator {

    private static Logger log = LogManager.getLogger(BeanF.class);
    @Value("${beanF.name}")
    @NonNull
    private String name;
    @Value("${beanF.value}")
    private int value;

    public BeanF() {
    }

    @Override
    public void validate() {
        if (getValue() >= 0) {
            log.info("Validate successful : A");
        } else {
            log.error("Validate error");
        }
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "BeanF{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
