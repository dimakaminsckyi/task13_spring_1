package com.epam.config;

import com.epam.model.bean.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.*;

@Configuration
@PropertySource("bean.properties")
@Import(BeanConfigTwo.class)
public class BeanConfigOne {

    @Bean
    @DependsOn(value = {"beanD", "beanB", "beanC"})
    public BeanA getBeanBC(BeanB getBeanB, BeanC getBeanC) {
        return new BeanA(getBeanB, getBeanC);
    }

    @Bean
    public BeanA getBeanBD(@Qualifier("beanB") BeanB beanB, @Qualifier("beanD") BeanD beanD) {
        return new BeanA(beanB, beanD);
    }

    @Bean
    public BeanA getBeanCD(@Qualifier("beanC") BeanC beanC, @Qualifier("beanD") BeanD beanD) {
        return new BeanA(beanC, beanD);
    }

    @Bean
    public BeanE getBeanE(BeanA getBeanBC) {
        return new BeanE(getBeanBC);
    }

    @Bean
    public BeanE getBeanE2(BeanA getBeanBD) {
        return new BeanE(getBeanBD);
    }

    @Bean
    public BeanE getBeanE3(@Qualifier("getBeanCD") BeanA bean) {
        return new BeanE(bean);
    }

    @Bean(name = "beanB", destroyMethod = "destroy", initMethod = "init")
    public BeanB getBeanB() {
        return new BeanB();
    }

    @Bean(name = "beanC", destroyMethod = "destroy", initMethod = "init")
    public BeanC getBeanC() {
        return new BeanC();
    }

    @Bean(name = "beanD", destroyMethod = "destroy", initMethod = "init")
    public BeanD getBeanD() {
        return new BeanD();
    }
}
